<div class="large-12 columns">
	<footer>
		<p>&copy; 
		<?php 
		$startYear = 2015;
		$thisYear = date('Y');
		if ($startYear == $thisYear) {
			echo $startYear;
		}
		else {
			echo "{$startYear}&ndash;{$thisYear}";
		}
		?> 
		Valdimar Gunnarsson</p>
	</footer>
</div>