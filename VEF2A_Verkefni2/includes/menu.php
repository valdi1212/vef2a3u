<nav class="top-bar" data-topbar role="navigation">
	<ul class="title-area">
		<li class="name">
			<h1><a href="index.php">Verkefni 2</a></h1>
		</li>
		<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	</ul>

	<section class="top-bar-section">
		<ul class="right">
			<li><a href="#">Right Button</a></li>
			<li class="has-dropdown">
				<a href="#">Right Dropdown Button</a>
				<ul class="dropdown">
					<li><a href="#">First Item</a></li>
					<li><a href="#">Second Item</a></li>
					<li><a href="#">Third Item</a></li>
				</ul>
			</li>
		</ul>

		<ul class="left">
			<li><a href="#">Left Button</a></li>
			<li><a href="#">Another Left Button</a></li>
		</ul>
	</section>
</nav>